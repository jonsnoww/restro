import React from "react";
import { Route, Routes } from "react-router-dom";
import { CartProvider } from "react-use-cart";
import DishState from "./components/context/DishState";
import { Header } from "./components/navbar/Navbar";
import { Home } from "./components/pages/Home";
import Invoice from "./components/pages/Invoice";
import { Menu } from "./components/pages/Menu";
import { Offers } from "./components/pages/Offers";
import { Order } from "./components/pages/Order";

const App = () => {
  return (
    <>
      <DishState>
        <CartProvider>
          <Header />
          <Routes>
            <Route path="/" exact element={<Home />} />
            <Route path="/Menu" exact element={<Menu />} />
            <Route path="/Offers" exact element={<Offers />} />
            <Route path="/Order" exact element={<Order />} />
            <Route path="/Invoice" exact element={<Invoice />} />
          </Routes>
        </CartProvider>
      </DishState>
    </>
  );
};

export default App;
