import React, { useContext, useRef } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import CardHeader from "react-bootstrap/esm/CardHeader";
import DishContext from "../context/DishContext";
import { useReactToPrint } from "react-to-print";

const Invoice = () => {
  const context = useContext(DishContext);
  const { orderdetails } = context;
  console.log(orderdetails);

  const componentRef = useRef();
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  return (
    <Container className="pt-5 print">
      <Row className="pt-5">
        <Col className="pt-2" xl={12}>
          <h5 className="text-center"> Order Placed</h5>
          <Card id="invoice" ref={componentRef}>
            <CardHeader>Invoice</CardHeader>
            <Card.Body>
              <div>
                <p>Date: {orderdetails.Date}</p>
                <p>Name: {orderdetails.Name}</p>
                <p>Eamil: {orderdetails.Email}</p>
                <p>Mobile: {orderdetails.Phone}</p>
                <p>Table No: {orderdetails.TableNo}</p>
                <hr></hr>
                <div className="d-flex justify-content-between text-center">
                  <p className="col-3  px-2 fw-bold">
                    <b style={{ fontSize: "13px" }} className="d-block m-auto">
                      Name
                    </b>
                  </p>
                  <p className="col-3 fw-bold">
                    <b style={{ fontSize: "13px" }} className="d-block m-auto">
                      Price
                    </b>
                  </p>
                  <p className="col-3 fw-bold">
                    <b style={{ fontSize: "13px" }} className="d-block m-auto">
                      Quantity
                    </b>
                  </p>
                  <p className="col-3 fw-bold">
                    <b style={{ fontSize: "13px" }} className="d-block m-auto">
                      Total
                    </b>
                  </p>
                </div>
                {orderdetails.Order.map((order) => {
                  return (
                    <div
                      key={order.id}
                      className="d-flex justify-content-between text-center"
                    >
                      <p className="col-3  px-2 fw-bold">
                        <b
                          style={{ fontSize: "13px" }}
                          className="d-block m-auto"
                        >
                          {order.DishName}
                        </b>
                      </p>
                      <p className="col-3 fw-bold">
                        <b
                          style={{ fontSize: "13px" }}
                          className="d-block m-auto"
                        >
                          {order.price}
                        </b>
                      </p>
                      <p className="col-3 fw-bold">
                        <b
                          style={{ fontSize: "13px" }}
                          className="d-block m-auto"
                        >
                          {order.quantity}{" "}
                        </b>
                      </p>
                      <p className="col-3 fw-bold">
                        <b
                          style={{ fontSize: "13px" }}
                          className="d-block m-auto"
                        >
                          {order.quantity * order.price}
                        </b>
                      </p>
                    </div>
                  );
                })}
                <hr></hr>
                <div className="d-flex">
                  <h5 className="col-8">Total</h5>
                  <h5 className="col-4 text-end">{orderdetails.Amount} Rs.</h5>
                </div>
              </div>
            </Card.Body>
          </Card>
          <Button className="mt-1 float-end" onClick={() => handlePrint()}>
            print
          </Button>
        </Col>
      </Row>
    </Container>
  );
};

export default Invoice;
