import React, { useContext } from "react";
import {
  Col,
  Container,
  Form,
  FormControl,
  InputGroup,
  Row,
} from "react-bootstrap";
import DishContext from "../context/DishContext";
import { useCart } from "react-use-cart";

export const Order = () => {
  const { items, cartTotal } = useCart();
  const context = useContext(DishContext);
  const { OrderSubmit, orderdetails, setOrderDetails } = context;

  const handleChange = (e) => {
    setOrderDetails({
      ...orderdetails,
      Amount: cartTotal,
      Order: items,
      Date: new Date().toLocaleString(),
      [e.target.name]: e.target.value,
    });
  };
  return (
    <Container className="pt-5">
      <Row className="pt-5">
        <Col className="pt-5" xl={12}>
          <h2 className="text-center">order info</h2>{" "}
        </Col>
        {items.length === 0 ? (
          <Col className="pt-5" xl={12}>
            <h2 className="text-center">Please Add items</h2>{" "}
          </Col>
        ) : (
          <Col xl={12}>
            <Form
              onSubmit={(e) => {
                e.preventDefault();
                OrderSubmit();
              }}
            >
              <InputGroup>
                <InputGroup.Text>Name</InputGroup.Text>
                <FormControl
                  name="Name"
                  onChange={(e) => handleChange(e)}
                  required
                ></FormControl>
              </InputGroup>
              <InputGroup className="my-3">
                <InputGroup.Text>Email</InputGroup.Text>
                <FormControl
                  type="email"
                  name="Email"
                  value={orderdetails.Email}
                  onChange={(e) => handleChange(e)}
                  required
                ></FormControl>
              </InputGroup>
              <InputGroup className="my-3">
                <InputGroup.Text>Phone No.</InputGroup.Text>
                <FormControl
                  type="number"
                  maxLength={10}
                  required
                  value={orderdetails.Phone}
                  name="Phone"
                  onChange={(e) => handleChange(e)}
                ></FormControl>
              </InputGroup>
              <InputGroup className="my-3">
                <InputGroup.Text>Amount</InputGroup.Text>
                <FormControl
                  type="number"
                  value={cartTotal}
                  maxLength={10}
                  name="Amount"
                  onChange={(e) => handleChange(e)}
                  required
                ></FormControl>
              </InputGroup>
              <InputGroup className="my-3">
                <InputGroup.Text>Table No.</InputGroup.Text>
                <FormControl
                  type="number"
                  name="TableNo"
                  value={orderdetails.TableNo}
                  onChange={(e) => handleChange(e)}
                  maxLength={1}
                  required
                ></FormControl>
              </InputGroup>
              <InputGroup className="my-3">
                <FormControl
                  style={{ background: "black", color: "white" }}
                  type="submit"
                  value={"Place Your Order"}
                  maxLength={1}
                  required
                ></FormControl>
              </InputGroup>
            </Form>
          </Col>
        )}
      </Row>
    </Container>
  );
};
