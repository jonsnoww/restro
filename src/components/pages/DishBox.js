import React from "react";
import { Button, Card, Col } from "react-bootstrap";
import CardHeader from "react-bootstrap/esm/CardHeader";
import { useCart } from "react-use-cart";

const DishBox = (props) => {
  const { addItem } = useCart();
  const item = props.dish;
  //   console.log(item);
  return (
    <Col key={item.id} xl={3}>
      <Card className={`my-2`}>
        <CardHeader>{item.Category}</CardHeader>
        <Card.Body>
          <img alt="food" className="w-100" src={item.DishImage} />
          <h3 className="my-2">{item.DishName}</h3>
          <p className="my-1">{item.DishInfo}</p>
          <div className="d-flex my-2 align-items-center">
            <div>
              <h5>Price:{item.price}Rs.</h5>
            </div>
            {item.Status && (
              <Button
                onClick={() => addItem(item)}
                style={{ marginLeft: "auto" }}
              >
                Add
              </Button>
            )}
            {!item.Status && (
              <Button
                // onClick={() => Cart(dish)}
                disabled
                style={{ marginLeft: "auto" }}
              >
                Not Avail.
              </Button>
            )}
          </div>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default DishBox;
