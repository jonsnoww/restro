import React, { useContext, useState } from "react";
import {
  Card,
  Col,
  Row,
  Container,
  Button,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import CardHeader from "react-bootstrap/esm/CardHeader";
import DishContext from "../context/DishContext";

export const Menu = () => {
  const context = useContext(DishContext);
  const { dishes, Cart } = context;
  const [query, setQuery] = useState("");
  const [data, setData] = useState(dishes);

  const SearchDishes = data.filter((item) => {
    return (
      item.DishName.toLowerCase().includes(query.toLowerCase()) ||
      item.Category.toLowerCase().includes(query.toLowerCase())
    );
  });

  return (
    <Container className="pt-5">
      <Row className="pt-5">
        <Col xl={12} className="my-3">
          <Container>
            <InputGroup>
              <InputGroup.Text>Search</InputGroup.Text>
              <FormControl
                onChange={(e) => {
                  setQuery(e.target.value);
                }}
                placeholder="Search Dish by Name and Category"
              ></FormControl>
            </InputGroup>
          </Container>
        </Col>
        {SearchDishes.length === 0 && (
          <h1 className="text-center">Not Found</h1>
        )}
        {SearchDishes.map((dish, index) => {
          return (
            <Col key={dish.key} xl={3}>
              <Card className={`my-2`}>
                <CardHeader>{dish.Category}</CardHeader>
                <Card.Body>
                  <img alt="food" className="w-100" src={dish.DishImage} />
                  <h3 className="my-2">{dish.DishName}</h3>
                  <p className="my-1">{dish.DishInfo}</p>
                  <div className="d-flex my-2 align-items-center">
                    <h5>Price:{dish.DishPrice}Rs.</h5>
                    <h5>{dish.Quantity}</h5>
                    {dish.Quantity === 0 ? (
                      <Button
                        // onClick={() => Cart(dish)}
                        onClick={() => {
                          dish.Quantity += 1;
                          console.log(dish.Quantity);
                        }}
                        style={{ marginLeft: "auto" }}
                      >
                        Add
                      </Button>
                    ) : (
                      <div className="d-flex">
                        <Button
                          onClick={() => {
                            dish.Quantity -= 1;
                            console.log(dish.Quantity);
                          }}
                        >
                          -
                        </Button>
                        {dish.Quantity}
                        <Button
                          onClick={() => {
                            dish.Quantity += 1;
                            console.log(dish.Quantity);
                          }}
                        >
                          +
                        </Button>
                      </div>
                    )}
                  </div>
                </Card.Body>
              </Card>
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};
