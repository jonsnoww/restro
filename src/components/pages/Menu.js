import React, { useContext, useState } from "react";
import { Col, Row, Container, InputGroup, FormControl } from "react-bootstrap";

import DishContext from "../context/DishContext";

import DishBox from "./DishBox";

export const Menu = () => {
  const context = useContext(DishContext);
  const { dishes } = context;
  const [query, setQuery] = useState("");
  // const [data, setData] = useState(dishes);

  const SearchDishes = dishes.filter((item) => {
    return (
      item.DishName.toLowerCase().includes(query.toLowerCase()) ||
      item.Category.toLowerCase().includes(query.toLowerCase())
    );
  });

  return (
    <Container className="pt-5">
      <Row className="pt-5">
        <Col xl={12} className="my-3">
          <Container>
            <InputGroup>
              <InputGroup.Text>Search</InputGroup.Text>
              <FormControl
                onChange={(e) => {
                  setQuery(e.target.value);
                }}
                placeholder="Search Dish by Name and Category"
              ></FormControl>
            </InputGroup>
          </Container>
        </Col>
        {SearchDishes.length === 0 && (
          <h1 className="text-center">Not Found</h1>
        )}
        {SearchDishes.map((dish, index) => {
          return <DishBox dish={dish} />;
        })}
      </Row>
    </Container>
  );
};
