import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import DishContext from "./DishContext";

const DishState = (props) => {
  const navigate = useNavigate();

  // const dishes = ;
  const [dishes, setDishes] = useState([
    {
      id: "1",
      DishName: "Burger",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Burger",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "2",
      DishName: "Momos",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Momos",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "3",
      DishName: "Shake",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Shake",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "4",
      DishName: "Pizza",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Pizza",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "5",
      DishName: "Tacos",
      price: 100,
      Category: "Tacos",
      quantity: 1,
      Status: true,
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "6",
      DishName: "Cake",
      price: 100,
      Category: "Cake",
      quantity: 1,
      Status: false,
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "7",
      DishName: "Sandwich",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Sandwich",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
    {
      id: "8",
      DishName: "Shake",
      price: 100,
      quantity: 1,
      Status: true,
      Category: "Shake",
      DishImage:
        "https://images.unsplash.com/photo-1571091718767-18b5b1457add?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8emluZ2VyJTIwYnVyZ2VyfGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      DishInfo:
        " Lorem ipsum dolor sit, amet consectetur adipisicing elit. Earumanimi suscipit consequatur ipsum soluta cum accusamus. Quo necessitatibus voluptatum aperiam!",
    },
  ]);

  // sum of cart

  const [orderdetails, setOrderDetails] = useState({
    Name: "",
    Email: "",
    Phone: "",
    Amount: "",
    TableNo: "",
    Order: "",
    Date: "",
  });

  const OrderSubmit = (e) => {
    setOrderDetails(orderdetails);
    navigate("/Invoice");
  };

  const PrintDiv = (value) => {
    // var backup = document.body.innerHTML;
    // var printDiv = document.getElementById(value).innerHTML;
    // // document.body.innerHTML = <html><head></head><body>{printDiv}</body></html>
    // console.log(
    //   <html>
    //     <head></head>
    //     <body>{printDiv}</body>
    //   </html>
    // );
    // window.print();
    // document.body.innerHTML = backup;
  };

  return (
    <DishContext.Provider
      value={{
        dishes,

        OrderSubmit,
        setOrderDetails,
        orderdetails,
        setDishes,
        PrintDiv,
      }}
    >
      {props.children}
    </DishContext.Provider>
  );
};

export default DishState;
