import React, { useState } from "react";
import { Navbar, Container, Nav, Button, Modal, Row } from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";

import { MdDelete } from "react-icons/md";
import { useCart } from "react-use-cart";
export const Header = () => {
  const {
    totalUniqueItems,
    items,
    cartTotal,
    updateItemQuantity,
    emptyCart,
    removeItem,
  } = useCart();

  const location = useLocation();
  const [show, setShow] = useState(false);
  // const [totalPrice, setTotalPrice] = useState(0);
  const handleClose = () => {
    setShow(false);
  };
  const handleShow = () => {
    setShow(true);
  };

  const NavbarStyle = {
    position: "fixed",
    zIndex: "999",
    width: "100%",
  };

  return (
    <>
      <Navbar style={NavbarStyle} bg="dark" variant="dark" expand="lg">
        <Container>
          <Navbar.Brand href="#home">mae</Navbar.Brand>
          <Navbar.Toggle
            style={{ marginLeft: "auto" }}
            aria-controls="basic-navbar-nav"
          />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="m-auto">
              <Nav.Link
                as={Link}
                className={`${location.pathname === "/" ? "active" : ""}`}
                to="/"
              >
                Home
              </Nav.Link>
              <Nav.Link
                as={Link}
                className={`${location.pathname === "/Menu" ? "active" : ""}`}
                to="/Menu"
              >
                Menu
              </Nav.Link>
              <Nav.Link
                as={Link}
                className={`${location.pathname === "/Offers" ? "active" : ""}`}
                to="/Offers"
              >
                Offers
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <Button className="mx-1" onClick={handleShow}>
            Cart ({totalUniqueItems})
          </Button>
        </Container>
      </Navbar>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton>
          <Modal.Title>Cart</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Row>
            <div className="d-flex justify-content-between text-center">
              <h6 className="col-3   px-2 fw-bold">
                <b style={{ fontSize: "13px" }} className="d-block m-auto">
                  Name
                </b>
              </h6>
              <h6 className="col-2 fw-bold">
                <b style={{ fontSize: "13px" }} className="d-block m-auto">
                  Price
                </b>
              </h6>
              <h6 className="col-3 fw-bold">
                <b style={{ fontSize: "13px" }} className="d-block m-auto">
                  Qty.
                </b>
              </h6>
              <h6 className="col-2 fw-bold">
                <b style={{ fontSize: "13px" }} className="d-block m-auto">
                  Total
                </b>
              </h6>
              <h6 className="col-1 fw-bold">
                <b
                  style={{ fontSize: "13px", color: "red" }}
                  className="d-block m-auto"
                >
                  <MdDelete />
                </b>
              </h6>
            </div>
            {items.length === 0 && (
              <h5 className="text-center py-3">No item </h5>
            )}
            <div className="py-3">
              {items.map((dish, index) => {
                return (
                  <div
                    key={dish.id}
                    className="d-flex  justify-content-between text-center align-items-baseline"
                  >
                    <h6 className="col-3  px-2 fw-bold">
                      <b
                        style={{ fontSize: "13px" }}
                        className="d-block m-auto"
                      >
                        {dish.DishName}
                      </b>
                    </h6>
                    <h6 className="col-2 fw-bold">
                      <b
                        style={{ fontSize: "13px" }}
                        className="d-block m-auto"
                      >
                        {dish.price}
                      </b>
                    </h6>
                    <h6 className="col-3 fw-bold">
                      <b
                        sstyle={{ fontSize: "13px" }}
                        className="d-flex justify-content-center"
                      >
                        <ul className="pagination set_quantity align-items-baseline">
                          <li className="page-item">
                            <button
                              className="page-link"
                              onClick={() =>
                                updateItemQuantity(dish.id, dish.quantity - 1)
                              }
                              style={{
                                border: "1px solid #DEE2E6",
                                borderTopLeftRadius: "0.25rem",
                                borderBottomLeftRadius: "0.25rem",
                              }}
                            >
                              <i className="fas fa-minus"></i>
                            </button>
                          </li>
                          {/* <li className="page-item">
                            <input
                              type="text"
                              name=""
                              className="page-link"
                              id="textbox"
                              value={dish.quantity}
                              onChange={() => {}}
                              style={{
                                border: "1px solid #DEE2E6",
                                width: "45px",
                              }}
                            />
                          </li> */}
                          <p className="mx-1">{dish.quantity}</p>
                          <li className="page-item">
                            <button
                              className="page-link"
                              onClick={() =>
                                updateItemQuantity(dish.id, dish.quantity + 1)
                              }
                              style={{
                                border: "1px solid #DEE2E6",
                                borderTopRightRadius: "0.25rem",
                                borderBottomRightRadius: "0.25rem",
                              }}
                            >
                              <i className="fas fa-plus"></i>
                            </button>
                          </li>
                        </ul>
                        {/* <Button size="sm" className="col-1">
                          +
                        </Button>
                        <Button size="sm" className="col-1">
                          {dish.quantity}
                        </Button>
                        <Button size="sm" className="col-1">
                          -
                        </Button> */}
                      </b>
                    </h6>
                    <h6 className="col-2 fw-bold">
                      <b
                        style={{ fontSize: "13px" }}
                        className="d-block m-auto"
                      >
                        {dish.price * dish.quantity}
                      </b>
                    </h6>
                    <h6 className="col-1 fw-bold">
                      <b
                        style={{
                          fontSize: "13px",
                          color: "red",
                          cursor: "pointer",
                        }}
                        className="d-block m-auto"
                        onClick={() => removeItem(dish.id)}
                      >
                        <MdDelete />
                      </b>
                    </h6>
                  </div>
                );
              })}
            </div>
            <hr />
            <div className="d-flex">
              <h5 className="col-8">Total</h5>
              <h5 className="col-4 text-end">Rs.{cartTotal}</h5>
            </div>
            <Link to="/Order">
              <Button className="w-100" onClick={handleClose}>
                Order
              </Button>
            </Link>
            {}
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            onClick={() => {
              emptyCart();
            }}
          >
            Clear All
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
